package com.raj.app;

public class Project {

	public String pcode;
	
	public Employee eob;

	@Override
	public String toString() {
		return "Project [pcode=" + pcode + ", eob=" + eob + "]";
	}

	public String getPcode() {
		return pcode;
	}

	public void setPcode(String pcode) {
		this.pcode = pcode;
	}

	public Employee getEob() {
		return eob;
	}

	public void setEob(Employee eob) {
		this.eob = eob;
	}
}
