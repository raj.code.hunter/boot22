package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
@Component
@Order(1)
public class BRunner implements CommandLineRunner{

	@Value("${my.app.host}")
	String host;
	@Value("${my.app.port}")
	Integer portN;
	@Value("${my.app.active}")
	Boolean active;
	@Value("${my.app.usrName}")
	String usrName;
	
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("BRunner.......");		
		System.out.println("BRunner......."+" host"+": "+ portN);
		System.out.println("BRunner.......");

		
	}

}
