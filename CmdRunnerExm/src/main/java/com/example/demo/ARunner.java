package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
@Component
@Order(-1)
public class ARunner implements CommandLineRunner {

	@Autowired
	AppConfig appconfig;
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("ARunner.......");
		System.out.println("Host......."+appconfig.getHost());
		System.out.println("Port......."+appconfig.getPort());
		System.out.println("isActive......."+appconfig.getActive());
		System.out.println("Username......."+appconfig.getUsrName());
		System.out.println("ARunner.......");


	}

	

}
