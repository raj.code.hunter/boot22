package com.raj.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Controller 

{
	@Autowired
	IEMPService sob;
	
	void getInfo() {
		System.out.println("Get info from controller ");
		sob.checkLogick();
	}

}
