package com.app;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Test {

	public static void main(String[] args) throws StreamReadException, DatabindException, IOException {
		// TODO Auto-generated method stub

		ObjectMapper mPr= new ObjectMapper();

		Employee emp=mPr.readValue(new File("src/main/resources/emp.json"), Employee.class);

		System.out.println(emp);
		
		
		List<Employee> empList=
				mPr.readValue(new File("src/main/resources/empList.json"), new TypeReference<List<Employee>>(){});


		System.out.println(empList.stream().filter(f->f.age>20).collect(Collectors.toList()));
	/**
	 * Read data from object and print in string format followed by Json Order
	 */
		String empWrite=mPr.writeValueAsString(emp);
		System.out.println("***********Json order **************");
		System.out.println(empWrite);
		
		/**
		 * Write the one object in empWriteFile 
		 */
		mPr.writeValue(new File("src/main/resources/empWriteFile.json"), emp);
		/**
		 * Write the List of the object in empWriteFile 
		 */

		mPr.writeValue(new File("src/main/resources/empWriteFileList.json"), empList);

				
	
	
	}

}
