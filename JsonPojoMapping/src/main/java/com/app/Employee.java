package com.app;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

//@JsonPropertyOrder("role")
@JsonPropertyOrder({"role","firstName"})
public class Employee {
	
	
	@JsonProperty("firstName")
	String firstNameR;
	
	String lastName;
	String role;
	Integer age;
	public String getFirstName() {
		return firstNameR;
	}
	public void setFirstName(String firstName) {
		this.firstNameR = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Integer getAge() {
		return age;
	}
	
	public void setAge(Integer age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Employee [firstName=" + firstNameR + ", lastName=" + lastName + ", role=" + role + ", age=" + age + "]";
	}
	
	

}
