package com.app.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@ComponentScan("com.app.test")
@EnableScheduling
public class AppConfig {

}
