package com.app.test;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ProductScheduleService {
	//@Value(" #{ 'Hello'.length() > 0 ? 55 : 44 }")
	//@Value(" #{ modelData.model.length() }")
	//@Value(" #{ model.model.toLowerCase() }") 
	@Value(" #{ T(java.lang.Math).abs( new java.util.Random().nextInt() ) }")
	public String codeData;
	
	
	@Scheduled(fixedDelay = 2000)
	public void getSchedule() {
		
		System.out.println("Hello: "+ " : "+ codeData +": "+ new Date());
		
	}
	
}
