package com.raj.test;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component
public class Model {
	@Value("TEST DATA INFORMATION")
	public String model;

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "Model [model=" + model + "]";
	}
	

}
