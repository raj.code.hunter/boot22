package com.raj.test;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Process {
	
	
	@Value("Raj")
	public String codeData;
	@Value("#{'Hello World'.length()}")
	//@Value("#{ 'Hello ABC'.length()  }")
	public Integer info;
	
	//@Value("#{'Hello fhjgjghj hkhkj lj World'.length()}")
	//@Value("#{ 'Hello ABC'.length()  }")
	@Value("#{model.model.toLowerCase()}")
	public String infoStr;
	
	//@Value("#{ 2 + 3 }")
		//@Value("#{ 'Hello ABC'.length()  }")
		//@Value("#{ new java.util.Random().nextInt() }")
		//@Value("#{ T(java.lang.Math).abs(-9)}")
		//@Value(" #{ T(java.lang.Math).abs( new java.util.Random().nextInt() ) }")
		@Value(" #{ 'Hello'.length() > 0 ? 55 : 44 }")
		//@Value(" #{ modelData.model.length() }")
		//@Value(" #{ model.model.toLowerCase() }") 
		public String code;
		
		//@Value("#{ false }")
		//@Value(" #{ new java.util.Random().nextInt() > 5 ? true:false}")
		//@Value(" #{ new java.util.Random().nextInt() > 5 }")
		//@Value(" #{ new java.util.Random().nextInt() ge 5 }")
		//@Value(" #{ 5 % 2 > 0 && 2 + 3 le 5  }")
		@Value(" #{ 5 % 2 > 0 and 2 + 3 le 5  }")
		private boolean isPresent;
	
	

	public String getCodeData() {
			return codeData;
		}

		public void setCodeData(String codeData) {
			this.codeData = codeData;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public boolean isPresent() {
			return isPresent;
		}

		public void setPresent(boolean isPresent) {
			this.isPresent = isPresent;
		}

	public String getInfoStr() {
		return infoStr;
	}

	public void setInfoStr(String infoStr) {
		this.infoStr = infoStr;
	}

	public Integer getInfo() {
		return info;
	}

	public void setInfo(Integer info) {
		this.info = info;
	}

	public String getId() {
		return codeData;
	}

	public void setId(String id) {
		this.codeData = id;
	}

	@Override
	public String toString() {
		return "Process [codeData=" + codeData + ", info=" + info + ", infoStr=" + infoStr + ", code=" + code
				+ ", isPresent=" + isPresent + "]";
	}
	
	

}
