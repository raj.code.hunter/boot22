package com.raj.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/*
 *  
 *  SpEL expression is created using : operators, variables,
 *  class/objects, method calls...etc returns finally one value
 *  Arithmetic  : +, -, *, /, %, ^, div, mod
 *  Relational  : <, >, ==, !=, <=, >=, lt, gt, eq, ne, le, ge
 *  Logical     : and, or, not, &&, ||, !
 *  Conditional : ?:

 */
public class Test {

public static void main (String args []) {
	
	@SuppressWarnings("resource")
	ApplicationContext ac= new AnnotationConfigApplicationContext(AppConfig.class);
	Process ps= ac.getBean("process",Process.class);
	System.out.println("PS: "+ps.codeData+"\ninfo : "+ps.info+" \nlength: " +ps.infoStr +"\nIsPresent: "+ps.isPresent()+" \ncode : "+ps.code); 
	
	
}
	
	

}
